Role Name
=========

Add a list of users to sudoers


Role Variables
--------------

You can find all variables in the `vars` and `defaults` directories. You can overwrite:

- sudoers_file (string) => filename under `/etc/sudoers.d` (default: `ansible_sudo_users`)
- no_passwd (boolean) => is the user able to sudo without password prompt (default: `true`)



Example Playbook
----------------

    - name: "Example playbook"
      hosts: all
      become: yes
      roles:
        - sudo
      vars:
        sudo_users:
          - user1
          - user2
          - user3


License
-------

MIT

Author Information
------------------
Romain CHANU  
UCBL1
